# LaraziCms Media 
This is a module in LaraziCms project, a very good CMS written in laravel framework. This module supports uploading images, files and managing images and files

```php
<img src="{{ zi_media_fit($path,500,600) }}" />
<img src="{{ zi_media_thumb($path,500,600) }}" />
<img src="{{ zi_media_crop($path,500,600) }}" />


ZiFileModel $file

<img src="{{ $file->link_crop(500,600) }}" />
<img src="{{ $file->link_fit(500,600) }}" />
<img src="{{ $file->link_thumb(500,600) }}" />

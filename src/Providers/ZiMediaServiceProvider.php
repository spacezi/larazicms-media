<?php

namespace ZiMedia\Providers;

use Illuminate\Support\Facades\File;
use ZiMedia\Console\Commands\CleanCache;
use Illuminate\Support\ServiceProvider;

class ZiMediaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CleanCache::class,
            ]);
        }

        $this->loadRoutesFrom(__DIR__ . '/../../routes/routes.php');
    }
}

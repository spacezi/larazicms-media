<?php

namespace ZiMedia\Controllers\MngMedia;

use Illuminate\Support\Facades\Storage;
use ZiBase\Helpers\ZiView;
use ZiBase\Models\ZiConfig;
use ZiBase\Models\ZiFileModel;


class MngMediaAction
{
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return ZiView::outputJsonErrorNotif(_lang("admin::global.function_not_found", "Function not found"));
        }
    }

    public function upload()
    {
        if (!zi_role_can('file.upload')) {
            return response(_lang("validation.account_cannot_access", "", [ "account" => auth()->user()?->account ]) . '. #' . strip_tags(_lang_without_sync("admin::permission.file.upload")), 442);
        }
        $max_upload_size = (float)config('zi.media.max_upload_size', 0);
        if ($max_upload_size <= 0) {
            $max_upload_size = 10 * 1024;
        }
        request()->validate([
            'collection' => 'nullable|string|min:1|max:100',
            'file'       => [ 'required', 'file', 'max:' . $max_upload_size, config('zi.media.mime_type', 'mimes:txt,jpeg,jpg,png,gif,pdf,doc,docx,xls,xlsx,ppt,pptx,zip,rar,json,yaml') ]
        ]);
        $uploadedFile = request()->file('file');
        $file_hash    = md5_file($uploadedFile->getRealPath());

        $file_path = date('Y/m/d/') . substr($file_hash, 0, 2) . '/' . $uploadedFile->getClientOriginalName();

        //Get config media
        $config   = ZiConfig::getConfigAndParserValue('media_config');
        $disk     = $config['disk'] ?? 'public';
        $uploaded = Storage::disk($disk)->putFileAs(
            '/',
            $uploadedFile,
            $file_path
        );

        if ($uploaded) {
            try {
                $ziFile             = new ZiFileModel();
                $ziFile->path       = $file_path;
                $ziFile->mime_type  = $uploadedFile->getClientMimeType();
                $ziFile->name       = $uploadedFile->getClientOriginalName();
                $ziFile->size       = $uploadedFile->getSize();
                $ziFile->file_hash  = $file_hash;
                $ziFile->disk       = $disk;
                $ziFile->collection = request('collection');
                $ziFile->save();
            } catch (\Exception $exception) {
                if (str_contains($exception->getMessage(), 'zi_file_file_hash_unique')) {
                    $ziFile = ZiFileModel::where('file_hash', $file_hash)->first();
                } else {
                    return ZiView::outputJsonException("UploadZiMediaError: " . $exception->getMessage());
                }
            }
            $ziFile->link = $ziFile->link();
            return response()->json($ziFile);
        } else {
            return response(_lang("admin::file.upload_error_text"), 442);
        }
    }
}

<?php

namespace ZiMedia\Controllers\MngMedia;

use Illuminate\Support\Facades\DB;
use ZiBase\Helpers\ZiView;
use ZiBase\Models\ZiFileModel;


class MngMediaView
{
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->list();
        }
    }

    public function list()
    {

        if(!zi_role_can("file.view")){
            return zi_view_not_permission('file.view');
        }
        $validateRule = [
            'q'         => 'nullable|strip_tags',
            'mime_type' => 'nullable|strip_tags',
            'created'   => 'nullable|strip_tags'
        ];
        zi_validate_listing($validateRule);
        $qBuilder = ZiFileModel::query();

        if ($q = request('q')) {
            $qBuilder->where(function ($query) use ($q) {
                return $query->orWhere('name', 'LIKE', '%' . $q . '%')->orWhere('path', 'LIKE', '%' . $q . '%');
            });
        }
        if ($q = request('mime_type')) {
            $qBuilder->where('mime_type', $q);
        }

        if ($q = request('created')) {
            $qBuilder->whereRaw("DATE_FORMAT(created_at, '%Y-%m') = ? ", [$q]);

        }

        $tpl['lsObj'] = $qBuilder->orderByDesc('id')->paginate(50);

        $lsFilterMineType        = ZiFileModel::select('mime_type', DB::raw('COUNT(id) as total'))->groupBy('mime_type')->get()->toArray();
        $tpl['lsFilterMineType'] = $lsFilterMineType;

        $lsFilterByMonth = ZiFileModel::select(
            DB::raw("DATE_FORMAT(created_at, '%Y-%m') AS month_year"),
            DB::raw('COUNT(id) as total')
        )->groupBy('month_year')->orderByDesc('month_year')->get()->toArray();

        $tpl['lsFilterByMonth'] = $lsFilterByMonth;

        return ZiView::setView(__DIR__, 'list', $tpl);
    }
    public function upload()
    {
        $tpl = [];
        return ZiView::setView(__DIR__, 'upload', $tpl);
    }
}

@extends('admin/_layouts.layouts-detached')
@section('title')
    {{_lang("admin::file.page_title")}}
@endsection
@section('content')
    @component('admin/_components.breadcrumb')
        @slot('title')
            {{_lang("admin::file.page_title")}}
        @endslot
        @slot('li')
            <li class="breadcrumb-item"><a href="{{routex('private.home')}}">{{_lang('admin::global.breadcrumb_home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{routex('private.file')}}">{{_lang("admin::file.breadcrumb")}}</a></li>
        @endslot
    @endcomponent

    @include('views/filter/quick-form')
    <div class="row row-cols-xxl-5 row-cols-lg-3 row-cols-md-2 row-cols-1">
        @foreach($lsObj as $item)
                <?php
                /**
                 * @var \ZiBase\Models\ZiFileModel $item
                 */
                ?>
            <div class="col">

                <div class="card mb-3 ribbon-box ribbon-fill ribbon-sm">
                    {{--<div class="ribbon ribbon-info"><i class="ri-flashlight-fill"></i></div>--}}
                    <div class="card-body">
                        <a class="d-flex align-items-center" target="_blank"
                           href="{!! $item->link() !!}">
                            <div class="flex-shrink-0">
                                @if($item->isImage())
                                    <img src="{{ $item->link_fit(120,120) }}" alt=""
                                         class="avatar-md rounded-3 me-2"/>
                                @else
                                    <div class="file-icon file-icon-lg" data-type="{{$item->extension()}}"></div>
                                @endif

                            </div>
                            <div class="flex-grow-1 ms-3">
                                <h6 class="fs-14 mb-1 text-truncate me-2">{{$item->name}}</h6>
                                <p class="text-muted mb-0">{{$item->sizeKB()}}k - <small>{{$item->disk}}</small></p>
                                <p class="text-muted mb-0"><small>{{$item->created_at}}</small></p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>
    <x-zi-paging :items="$lsObj"></x-zi-paging>

@endsection
@push('script')

@endpush

<form>
    <div class="card">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-xl-3 col-sm-12 col-md-6">
                  <x-zi-input-search></x-zi-input-search>
                </div>
                <div class="col-xl-3 col-sm-12 col-md-6">
                    <select class="form-select" name="mime_type">
                        <option value="0">{{_lang("admin::file.mime_type")}}</option>
                        @foreach($lsFilterMineType as $key=>$value)
                            <option value="{{$value['mime_type']}}" @if(request('mime_type')==$value['mime_type']) selected @endif>{{$value['mime_type']}} ({{$value['total']}})</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-3  col-sm-12 col-md-6">
                    <select class="form-select" name="created">
                        <option value="0">{{_lang("admin::global.created_at")}}</option>
                        @foreach($lsFilterByMonth as $key=>$value)
                            <option value="{{$value['month_year']}}" @if(request('created')==$value['month_year']) selected @endif>{{$value['month_year']}} ({{$value['total']}})</option>
                        @endforeach
                    </select>
                </div>
                <!--end col-->
                <div class="col-xl-3 col-sm-12  col-md-6 ms-auto d-flex justify-content-end align-items-center">
                    <div class="btn-group ms-2">
                        <button type="submit" class="btn btn-primary waves-light d-inline-flex align-items-center"><i class="ri-search-line me-1"></i>{{_lang('admin::global.search',"Search")}}</button>
                    </div>
                    <div class="btn-group ms-2">
                        <a href="{{routex('private.file',['function'=>'upload'])}}" class="btn btn-soft-primary waves-light d-inline-flex align-items-center"><i class="mdi mdi-upload me-1"></i>{{_lang('admin::global.upload',"Upload")}}</a>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
    </div>
</form>

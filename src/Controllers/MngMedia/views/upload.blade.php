@extends('admin/_layouts.layouts-detached')
@section('title')
    {{_lang("admin::file.page_upload_title")}}
@endsection
@section('content')
    @component('admin/_components.breadcrumb')
        @slot('title')
            {{_lang("admin::file.page_upload_title")}}
        @endslot
        @slot('li')
            <li class="breadcrumb-item"><a href="{{routex('private.home')}}">{{_lang('admin::global.breadcrumb_home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{routex('private.file')}}">{{_lang("admin::file.breadcrumb")}}</a></li>
        @endslot
    @endcomponent
    <div class="btn-group mb-3">
        <a href="{{routex('private.file')}}" class="btn btn-soft-primary waves-light d-inline-flex align-items-center">
            <span class="mdi mdi-share" style=" transform: rotate(205deg);"></span>
            {{_lang('admin::menu.file')}}</a>
    </div>

    <x-zi-box-upload-image-multiple :files="$obj->files??[]"></x-zi-box-upload-image-multiple>
@endsection
@push('script')
@endpush

<?php

use Illuminate\Support\Facades\Route;
use ZiMedia\Controllers\MngMedia\MngMediaAction;
use ZiMedia\Controllers\MngMedia\MngMediaView;

Route::group(
    [
        'middleware' => [ 'web', 'system4admin', 'backend', '2fa' ],
        'prefix'     => 'private',
    ], function () {

    Route::group([ 'prefix' => 'file' ], function () {
        Route::get('/{function?}', [ MngMediaView::class, 'index' ])->name("private.file");
        Route::match([ 'post', 'delete' ], '/{function?}', [ MngMediaAction::class, 'index' ])->name("private.file.action");
    });
});
